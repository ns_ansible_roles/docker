Роль для установки Docker в ОС Ubuntu

Добавляем роль в requirements.yml

```
- name: docker
  src: git+https://gitlab.com/ns_ansible_roles/docker
  version: master
```
Устанавливаем роль

```
ansible-galaxy install -r requirements.yml
```

Пример плейбука:

```
- hosts: servers
  gather_facts: true
  become: true
  roles:
    - docker
```
